%option noyywrap
%option yylineno
%option nounput
%{
#include "minako.h"
#include <string.h>

%}

%x LINE_COMMENT
%x BLOCK_COMMENT
%x STRING
WHITESPACE  [ \t\n\r]
EXTRACHAR   [\+\-\*\/\(\)\{\},;:=]
ziffer		[[:digit:]]
buchstabe	[[:alpha:]]
integer            ({ziffer})+
float              ({integer}"."{integer})|("."{integer})

%%
bool                                                                  {return KW_BOOLEAN;}
do                                                                    {return KW_DO;}
else                                                                  {return KW_ELSE;}
float                                                                 {return KW_FLOAT;}
for                                                                   {return KW_FOR;}
if                                                                    {return KW_IF;}
int                                                                   {return KW_INT;}
printf                                                                {return KW_PRINTF;}
return                                                                {return KW_RETURN;}
void                                                                  {return KW_VOID;}
while                                                                 {return KW_WHILE;}


"=="                                                                    {return EQ;}
"!="                                                                    {return NEQ;}
"<"                                                                     {return LSS;}
">"                                                                     {return GRT;}
"<="                                                                    {return LEQ;}
">="                                                                    {return GEQ;}
"&&"                                                                    {return AND;}
"||"                                                                    {return OR;}




[-+]?{float}([eE][-+]?{integer})?|{integer}[eE][-+]?{integer}           { yylval.floatValue = (float) strtod(yytext, NULL); return CONST_FLOAT;}
{integer}                                                               { yylval.intValue = atoi(yytext);  return CONST_INT;}

true                                                                    { yylval.intValue = 1; return CONST_BOOLEAN;}
false                                                                   { yylval.intValue = 0; return CONST_BOOLEAN;}


[\"]({buchstabe}|{ziffer}|{WHITESPACE})*[\"]                            { yylval.string=strndup(yytext+1,strlen(yytext)-2);return CONST_STRING;}


{buchstabe}({buchstabe}|{ziffer})*                                      { yylval.string = strdup(yytext); return ID; }



"/*"                                                                      {BEGIN(BLOCK_COMMENT);}
<BLOCK_COMMENT>.|\n                                                        {}
<BLOCK_COMMENT>"*/"                                                        {BEGIN(INITIAL);}
"//"                                                                      {BEGIN(LINE_COMMENT);}
<LINE_COMMENT>.                                                            {}
<LINE_COMMENT>\n                                                           {BEGIN(INITIAL);}


{WHITESPACE}+                                                             {}
{EXTRACHAR}                                                               {return (int) yytext[0];}
<<EOF>>                                                                   {return -1;}
%%